-module(notify_handler).
-export([init/3]).
-export([websocket_init/3,websocket_handle/3,
    websocket_info/3,websocket_terminate/3]).

init({_,http}, _Req, _Opts) ->
    {upgrade,protocol,cowboy_websocket}.

websocket_init(_TransName, Req, [UdpPort]) ->
	{ok,Sock} = gen_udp:open(UdpPort, [{active,true},{reuseaddr,true}]),
	{ok,Req,Sock}.

websocket_handle(_Data, Req, St) ->
    {ok,Req,St}.

websocket_info({udp,Sock,_RemHost,_RemPort,Data}, Req, Sock =St) ->
	{reply,{text,Data},Req,St};
websocket_info(_Info, Req, St) ->
    {ok,Req,St}.

websocket_terminate(_Reason, _Req, Sock =_St) ->
	ok = gen_udp:close(Sock),
    ok.

%%EOF
