-module(start_handler).
-export([init/3]).
-export([handle/2,terminate/3]).

-define(GATOR_HOST, {192,168,0,1}).

init({tcp,http}, Req, []) ->
	{ok,Req,[]}.

handle(Req, St) ->
	{ok,Bin,_} = cowboy_req:body(Req),
	[S1,S2] = string:tokens(binary_to_list(Bin), "|"),
	
	RingSize = list_to_integer(S1),
	Message = list_to_binary(S2),

	GOpts = [{host,?GATOR_HOST}],
	{ok,Stale} = egator:list(<<"circling">>, GOpts),
	lists:foreach(fun({Dom,_}) ->
		case egator:destroy(Dom, GOpts) of
		ok ->
			io:format("Stale domain ~s destroyed~n", [Dom]);
		_ ->
			io:format("*** Cannot destroy ~s, ignored~n", [Dom])
		end
	end, Stale),

	SeedName = <<"circling1">>,
	SeedImage = <<"/root/images/circling.img">>,
	Extra = list_to_binary(io_lib:format("-seed ~w ~s -domain ~s "
			"-ipaddr 192.168.2.2 -netmask 255.255.255.0 -gateway 192.168.2.1 "
			"-root /erlang -pz /circling/ebin -pz /circling/deps/egator/ebin "
			"-home /circling -s circling_app",
				[RingSize,Message,SeedName])),

	_Ok = egator:create(SeedName, SeedImage,
			[{memory,32},{extra,Extra},{bridge,<<"br2">>}], GOpts),

	{ok,Reply} = cowboy_req:reply(200, [], Req),
	{ok,Reply,St}.

terminate(_How, _Req, _St) ->
	ok.

%%EOF
