-module(zerogate_app).

-behaviour(application).

-define(NOTIFY_PORT, 9010).

%% Application callbacks
-export([start/2, stop/1]).
-export([start/0]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->

	application:start(crypto),
	application:start(ranch),
	application:start(cowboy),

	MimeTypes = [{<<".html">>,[<<"text/html">>]},
		{<<".css">>,[<<"text/css">>]},
		{<<".js">>,[<<"application/javascript">>]},
		{<<".png">>,[<<"image/png">>]}],

	Dispatch = cowboy_router:compile([
		{'_',[
			{"/notify",notify_handler,[?NOTIFY_PORT]},
			{"/start",start_handler,[]},
			{"/static/[...]",cowboy_static,[{directory,"priv/www"},
											{mimetypes,MimeTypes}]}
		]}
	]),

	{ok,_} = cowboy:start_http(www, 8,
		[{port,9090}],
		[{env,[{dispatch,Dispatch}]}]
	),

	application:start(zerogate).


start(_StartType, _StartArgs) ->
    zerogate_sup:start_link().

stop(_State) ->
    ok.

%%EOF
